



module.exports = (sequelize, Sequelize) => {
  const User = sequelize.define("user", {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
      primaryKey: true,
      // autoIncrement: true,
      // allowNull: false
    },
    person_id: {
      type: Sequelize.STRING
    },
    user_id: {
      type: Sequelize.STRING
    },
    username: {
      type: Sequelize.STRING,
      allowNull : false
    },
    fullname: {
      type: Sequelize.STRING,
      allowNull : false
    },
    email: {
      type: Sequelize.STRING,
      allowNull : false
    },
    password: {
      type: Sequelize.STRING,
      allowNull : false
    },
    createdBy: {
      type: Sequelize.STRING
    },
    updatedBy: {
      type: Sequelize.STRING
    }
  });

  const Role = sequelize.define("role", {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
      primaryKey: true,
      // autoIncrement: true,
      // allowNull: false
    },
    role_uuid: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
      // allowNull: false
    },
    name: {
      type: Sequelize.STRING
    }
  });

  const UserRole = sequelize.define("user_role", {
    user_id: {
      type: Sequelize.INTEGER
    },
    role_id: {
      type: Sequelize.INTEGER
    }
  });

  User.belongsToMany(Role, {
    through: "user_role",
    as: "Roles",
    foreignKey: "user_id",
  });

  Role.belongsToMany(User, {
    through: "user_role",
    as: "Users",
    foreignKey: "role_id",
  });

  return {User, Role, UserRole};
};