const dbConfig = require("../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: false,

  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

const Tutorial = require("./tutorial.model.js")(sequelize, Sequelize);
const {User, Role, UserRole} = require("./user.model.js")(sequelize, Sequelize);
const {Person} = require("./person.model.js")(sequelize, Sequelize);
const {Receipt} = require("./cashflow/receipt.model.js")(sequelize, Sequelize);
const {Transaction} = require("./cashflow/transaction.model.js")(sequelize, Sequelize);
const {Invoice} = require("./cashflow/invoice.model.js")(sequelize, Sequelize);
const { CashflowConfig, PeriodGlobal, ConfigGlobal, ValueGlobal } = require("./configuration.model.js")(sequelize, Sequelize);

db.tutorials = Tutorial

db.user = User
db.role = Role
db.user_role = UserRole
db.person = Person

db.receipt = Receipt
db.transaction = Transaction
db.invoice = Invoice

db.cashflowconfig = CashflowConfig
db.periodglobal = PeriodGlobal
db.configglobal = ConfigGlobal
db.valueglobal = ValueGlobal
// db.user = require("./user.model.js")(sequelize, Sequelize);

db.person.hasOne(db.user, {
  onDelete: 'NO ACTION',
  onUpdate: 'CASCADE'
});

db.user.belongsTo(db.person);

db.receipt.hasMany(db.transaction, {
  onDelete: 'NO ACTION',
  onUpdate: 'NO ACTION'
});

db.transaction.belongsTo(db.receipt);

db.person.hasMany(db.transaction, {
  onDelete: 'NO ACTION',
  onUpdate: 'NO ACTION'
});

db.transaction.belongsTo(db.person);

db.person.hasMany(db.receipt, {
  onDelete: 'NO ACTION',
  onUpdate: 'NO ACTION'
});

db.receipt.belongsTo(db.person);

db.person.hasMany(db.invoice, {
  onDelete: 'NO ACTION',
  onUpdate: 'NO ACTION'
});

db.invoice.belongsTo(db.person);

db.invoice.hasMany(db.transaction, {
  onDelete: 'NO ACTION',
  onUpdate: 'NO ACTION'
});

db.transaction.afterBulkCreate(async (txn, options) => {
  for(let i = 0; i < txn.length; i++){
    if(txn[i].invoice_id_doc){
      db.invoice.update(
        { status: sequelize.literal('(case when amount = (amount_pay+'+txn[i].amount+') then "SUDAH LUNAS" else "BELUM LUNAS" end)'), 
        amount_pay: sequelize.literal('amount_pay + '+txn[i].amount),
        },
        { where: { id: txn[i].invoice_id_doc } }
      );
    }
  }
});

module.exports = db;
