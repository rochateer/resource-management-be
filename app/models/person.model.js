



module.exports = (sequelize, Sequelize) => {
    const Person = sequelize.define("person", {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        // autoIncrement: true,
        // allowNull: false
      },
      user_id: {
        type: Sequelize.STRING
      },
      person_id: {
        type: Sequelize.STRING
      },
      fullname: {
        type: Sequelize.STRING,
        allowNull : false
      },
      email: {
        type: Sequelize.STRING
      },
      phone: {
        type: Sequelize.STRING
      },
      province: {
        type: Sequelize.STRING
      },
      city: {
        type: Sequelize.STRING
      },
      kecamatan: {
        type: Sequelize.STRING
      },
      kelurahan: {
        type: Sequelize.STRING
      },
      address: {
        type: Sequelize.TEXT
      },
      person_type: {
        type: Sequelize.STRING
      },
      level: {
        type: Sequelize.STRING
      },
      specific_level: {
        type: Sequelize.STRING
      },
      nik: {
        type: Sequelize.STRING
      },
      jenis_kelamin: {
        type: Sequelize.STRING
      },
      berat_badan: {
        type: Sequelize.DOUBLE
      },
      tinggi_badan: {
        type: Sequelize.DOUBLE
      },
      birth_place: {
        type: Sequelize.STRING
      },
      birth_day: {
        type: Sequelize.DATEONLY
      },
      age: {
        type: Sequelize.INTEGER
      },
      golongan_darah: {
        type: Sequelize.STRING
      },
      asal_sekolah: {
        type: Sequelize.STRING
      },
      name_ayah: {
        type: Sequelize.STRING
      },
      name_ibu: {
        type: Sequelize.STRING
      },
      pekerjaan_ayah: {
        type: Sequelize.STRING
      },
      pekerjaan_ibu: {
        type: Sequelize.STRING
      },
      birth_place_ayah: {
        type: Sequelize.STRING
      },
      birth_place_ibu: {
        type: Sequelize.STRING
      },
      birth_day_ayah: {
        type: Sequelize.STRING
      },
      birth_day_ibu: {
        type: Sequelize.STRING
      },
      pendidikan_ayah: {
        type: Sequelize.STRING
      },
      pendidikan_ibu: {
        type: Sequelize.STRING
      },
      phone_ayah: {
        type: Sequelize.STRING
      },
      phone_ibu: {
        type: Sequelize.STRING
      },
      address_ayah: {
        type: Sequelize.TEXT
      },
      address_ibu: {
        type: Sequelize.TEXT
      },
      createdBy: {
        type: Sequelize.STRING
      },
      updatedBy: {
        type: Sequelize.STRING
      }
    });
  
    return {Person};
  };