



module.exports = (sequelize, Sequelize) => {
    const Receipt = sequelize.define("receipt", {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        // autoIncrement: true,
        // allowNull: false
      },
      receipt_type: {
        type: Sequelize.STRING
      },
      receipt_number: {
        type: Sequelize.STRING
      },
      total_amount: {
        type: Sequelize.DOUBLE,
        defaultValue: 0,
      },
      client_id: {
        type: Sequelize.STRING
      },
      client_name: {
        type: Sequelize.STRING
      },
      remark: {
        type: Sequelize.STRING
      },
      period: {
        type: Sequelize.STRING
      },
      receipt_actual_date: {
        type: Sequelize.DATE
      },
      receiver: {
        type: Sequelize.STRING
      },
      deleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      createdBy: {
        type: Sequelize.STRING
      },
      updatedBy: {
        type: Sequelize.STRING
      },
    });
  
    // db.tutorials.hasMany(db.comments, { as: "comments" });

    // db.comments.belongsTo(db.tutorials, {
    //   foreignKey: "tutorialId",
    //   as: "tutorial",
    // });
  
    return { Receipt };
  };