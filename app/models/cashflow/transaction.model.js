module.exports = (sequelize, Sequelize) => {
    const Transaction = sequelize.define("transaction", {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        // autoIncrement: true,
        // allowNull: false
      },
      receipt_id : {
        type: Sequelize.STRING
      },
      name: {
        type: Sequelize.STRING
      },
      group_transaction_type: {
        type: Sequelize.STRING
      },
      cashflow_type: {
        type: Sequelize.STRING
      },
      type_id: {
        type: Sequelize.STRING
      },
      type: {
        type: Sequelize.STRING
      },
      special_type: {
        type: Sequelize.STRING
      },
      transaction_actual_date: {
        type: Sequelize.DATE
      },
      client_id: {
        type: Sequelize.STRING
      },
      client_name: {
        type: Sequelize.STRING
      },
      amount: {
        type: Sequelize.DOUBLE,
        defaultValue: 0,
      },
      remark: {
        type: Sequelize.STRING
      },
      invoice_id_doc: {
        type: Sequelize.STRING,
      },
      invoice_id: {
        type: Sequelize.STRING,
      },
      transaction_period: {
        type: Sequelize.STRING
      },
      transaction_year: {
        type: Sequelize.STRING
      },
      transaction_month: {
        type: Sequelize.STRING
      },
      period: {
        type: Sequelize.STRING
      },
      deleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      receiver: {
        type: Sequelize.STRING
      },
      createdBy: {
        type: Sequelize.STRING
      },
      updatedBy: {
        type: Sequelize.STRING
      }
    });
  
    return { Transaction };
  };