module.exports = (sequelize, Sequelize) => {
    const Invoice = sequelize.define("invoice", {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
      },
      invoice_id: {
        type: Sequelize.STRING
      },
      client_id: {
        type: Sequelize.STRING
      },
      client_name: {
        type: Sequelize.STRING
      },
      name: {
        type: Sequelize.STRING
      },
      type_id: {
        type: Sequelize.STRING
      },
      type: {
        type: Sequelize.STRING
      },
      special_type: {
        type: Sequelize.STRING
      },
      amount: {
        type: Sequelize.DOUBLE,
        defaultValue: 0,
      },
      amount_pay: {
        type: Sequelize.DOUBLE,
        defaultValue: 0,
      },
      invoice_period: {
        type: Sequelize.STRING
      },
      invoice_year: {
        type: Sequelize.STRING
      },
      invoice_month: {
        type: Sequelize.STRING
      },
      status: {
        type: Sequelize.STRING
      },
      remark: {
        type: Sequelize.STRING
      },
      period: {
        type: Sequelize.STRING
      },
      deleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      createdBy: {
        type: Sequelize.STRING
      },
      updatedBy: {
        type: Sequelize.STRING
      }
    });
  
    return { Invoice };
  };