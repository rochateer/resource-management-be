module.exports = app => {
    const transactions = require("../controllers/cashflow.controller.js");
    const invoice = require("../controllers/invoice.controller.js");
  
    var router = require("express").Router();
  
    // Create a new Tutorial
    router.get("/transaction/receipt/list", transactions.getReceipt);
    router.get("/transaction/receipt/detail/:id", transactions.getReceiptById);
    router.get("/transaction/cashflow-transaction/list", transactions.getTransaction);
    router.get("/transaction/cashflow-transaction/detail/:id", transactions.getTransactionById);

    router.get("/invoice/list/", invoice.get);
    router.get("/invoice/detail/:id", invoice.getById);
  
    router.post("/transaction/receipt/create", transactions.createReceipt);
    router.post("/invoice/create", invoice.createInvoice);
    router.post("/invoice/createManyUploader", invoice.createBulkInvoiceUploader);
  
    app.use('/api', router);
  };
    