module.exports = app => {
  const users = require("../controllers/user.controller.js");
  const roles = require("../controllers/role.controller.js");

  var router = require("express").Router();

  // Create a new Tutorial
  router.get("/user/", users.get);
  router.get("/user/:id", users.getById);
  router.get("/role/", roles.get);

  router.post("/user/create", users.create);
  router.post("/role/create", roles.create);
  router.post("/role/create-many", roles.createBulk);

  router.patch("/user/add-role", users.addRole);
  router.patch("/user/add-role-multi", users.addManyRole);

  app.use('/api', router);
};
  