module.exports = app => {
    const persons = require("../controllers/person.controller.js");
  
    var router = require("express").Router();
  
    // Create a new Tutorial
    router.get("/person/", persons.get);
    router.get("/person/:id", persons.getById);
  
    router.post("/person/create", persons.create);
  
    app.use('/api', router);
  };
    