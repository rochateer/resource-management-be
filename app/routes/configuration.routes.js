module.exports = app => {
  const configuration = require("../controllers/configuration.controller.js");

  var router = require("express").Router();

  // Create a new Tutorial
  router.get("/config/cashflow/", configuration.getCashflowConfig);
  router.get("/config/value-global/", configuration.getValueGlobal);

  router.post("/config/cashflow/create", configuration.createCashflowConfig);
  router.post("/config/value-global/create", configuration.createValueGlobal);

  app.use('/api', router);
};
  