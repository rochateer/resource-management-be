const db = require("../models");
const Person = db.person;
const Op = db.Sequelize.Op;
const {bodyJsonGet, queryApi} = require("../component/standardComponent")

// Create and Save a new Tutorial
const create = (req, res) => {

  const body_data = req.body.data;

  // Validate request
  if (!body_data.fullname) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  // Create a Tutorial
  const person = {
    ...body_data,
    createdBy : "admintc@gmail.com",
    updatedBy : "admintc@gmail.com"
  };

  // person["birth_day"] = new Date(person.birth_day)
  // person["birth_day_ayah"] = new Date(person.birth_day_ayah)
  // person["birth_day_ibu"] = new Date(person.birth_day_ibu)

  // res.status(200).send({data: person})

  // Save Tutorial in the database
  Person.create(person)
    .then(data => {
      res.status(200).send({data: data})
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Tutorial."
      });
    });
};

const get = (req, res) => {

  const {query_find, limit, page, options} = queryApi(req.query);

  Person.findAndCountAll({ ...options, where: query_find })
    .then(d => {
      const data = bodyJsonGet(d, options['limit'], page)
      res.status(200).send({ ...data })
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
};

const getById = (req, res) => {

  const userId = req.params.id;

  Person.findByPk(userId, {include: 'user'})
    .then(data => {
      res.status(200).send({data: data})
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
};

module.exports = {
    create : create,
    get : get,
    getById : getById
  }