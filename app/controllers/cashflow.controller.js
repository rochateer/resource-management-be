const db = require("../models");
const Receipt = db.receipt;
const Transaction = db.transaction;
const Person = db.person;
const Op = db.Sequelize.Op;
const {parsingDate, getPeriod} = require("../component/functionComponent");
const {bodyJsonGet, queryApi} = require("../component/standardComponent");

const createReceipt = async(req, res) => {

  const body_data = req.body.data;

  // Validate request
  if (!body_data.total_amount) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  if(body_data.receipt_form_type === "Pembayaran Siswa"){
    const data_person = await Person.findOne({ where: { id: body_data.person_id_doc } });
    if (!data_person) {
      res.status(404).send({
        message:
          err.message || "Civitas data not found"
      });
    }
  }

  const date_now = new Date()

  const parsing_date = parsingDate(date_now);

  const random_number = Math.floor(Math.random() * 100);

  const receipt_number = "TC"+"-"+parsing_date.yr+parsing_date.month+parsing_date.date+"-"+random_number.toString().padStart(4, '0');

  let period_now = await getPeriod();

  let transactionDataOps = [];
  let receiptId = null;

  let creator = "admintc@gmail.com"

  const data_receipt = {
    receipt_type: body_data.receipt_form_type,
    receipt_number: receipt_number,
    total_amount: body_data.total_amount,
    personId: body_data.person_id_doc,
    client_id: body_data.person_id,
    client_name: body_data.fullname,
    remark: body_data.remark,
    period: period_now,
    receipt_actual_date: body_data.actual_date,
    receiver: creator,
    createdBy: creator,
    updatedBy: creator
  }

  const transaction_data  = body_data.transaction_data

  try{

    const receipt_ops = await Receipt.create(data_receipt);
    receiptId = receipt_ops.id

    // receiptId = "123124123"

    if(receiptId){

      for(let i = 0; i < transaction_data.length; i++){

        const transaction_idx = {
          receipt_id : receipt_number,
          name: transaction_data[i].transaction_name,
          group_transaction_type: transaction_data[i].group_poin_pembayaran,
          cashflow_type: body_data.receipt_form_type,
          type_id: transaction_data[i].transaction_type_id,
          type: transaction_data[i].transaction_type,
          special_type: transaction_data[i].transaction_special_type,
          transaction_actual_date: body_data.actual_date,
          client_id: body_data.person_id,
          client_name: body_data.fullname,
          amount: transaction_data[i].transaction_amount,
          invoice_id_doc: transaction_data[i].invoice_id_doc,
          invoice_id: transaction_data[i].invoice_id,
          transaction_period: transaction_data[i].transaction_period,
          transaction_year: transaction_data[i].transaction_year,
          transaction_month: transaction_data[i].transaction_month,
          period: transaction_data[i].period,
          personId: body_data.person_id_doc,
          invoiceId: transaction_data[i].invoice_id_doc,
          receiptId: receiptId,
          receiver: creator,
          createdBy: creator,
          updatedBy: creator
        }

        if(!transaction_idx.transaction_period){
          let pre_transaction_period = [transaction_idx.transaction_month, transaction_idx.transaction_year]
          pre_transaction_period = pre_transaction_period.filter(tr => tr);
          pre_transaction_period = pre_transaction_period.join("#");

          transaction_idx["transaction_period"] = pre_transaction_period
        }
        
        if(!transaction_idx.name){
          let pre_transaction_name = [transaction_idx.type, transaction_idx.transaction_period, body_data.person_id]
          pre_transaction_name = pre_transaction_name.filter(tr => tr)
          pre_transaction_name = pre_transaction_name.join("-")

          transaction_idx["name"] = pre_transaction_name;
        }

        transactionDataOps.push(transaction_idx)

      }
      
    }

    const opsTransaction = await Transaction.bulkCreate(transactionDataOps)

    res.status(200).send({data: receipt_ops, data_transaction : opsTransaction})

    // res.status(200).send({data: data_receipt, data_transaction : transactionDataOps})

  }catch(err){

    Receipt.destroy({
        where: { id : receiptId}
    })
    
    res.status(500).send({
      message:
        err.message || "Some error occurred while creating the Tutorial."
    });
  }
}

const getReceipt = (req, res) => {

  const {query_find, limit, page, options} = queryApi(req.query);

  Receipt.findAndCountAll({ ...options, where: query_find })
    .then(d => {
      const data = bodyJsonGet(d, options['limit'], page)
      res.status(200).send({ ...data })
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
};

const getReceiptById = (req, res) => {

  const dataId = req.params.id;

  Receipt.findByPk(dataId, {include: ['transactions']})
    .then(data => {
      res.status(200).send({data: data})
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
};

const getTransaction = (req, res) => {
  const client_name = req.query.client_name;
  var condition = client_name ? { client_name: { [Op.like]: `%${client_name}%` } } : null;

  Transaction.findAll({ where: condition })
    .then(data => {
      res.status(200).send({data: data})
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
};

const getTransactionById = (req, res) => {
  
  const dataId = req.params.id;

  Transaction.findByPk(dataId)
    .then(data => {
      res.status(200).send({data: data})
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
};

module.exports = {
  createReceipt,
  getReceipt,
  getReceiptById,
  getTransaction,
  getTransactionById
}