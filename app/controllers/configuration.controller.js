const db = require("../models");
const CashflowConfig = db.cashflowconfig;
const PeriodGlobal = db.periodglobal;
const ConfigGlobal = db.configglobal;
const ValueGlobal = db.ValueGlobal;
const Op = db.Sequelize.Op;
const {bodyJsonGet, queryApi} = require("../component/standardComponent");

const createCashflowConfig = async(req, res) => {

  const body_data = req.body.data;

  // Validate request
  if (!body_data.type) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  // Create a Tutorial
  const config = {
    ...body_data
  };

  CashflowConfig.create(config)
    .then(data => {
      res.status(200).send({data: data})
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Tutorial."
      });
    });
};

const createValueGlobal = async(req, res) => {

  const body_data = req.body.data;

  // Validate request
  if (!body_data.type) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  // Create a Tutorial
  const config = {
    ...body_data
  };

  ValueGlobal.create(config)
    .then(data => {
      res.status(200).send({data: data})
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Tutorial."
      });
    });
};

const getCashflowConfig = (req, res) => {

  const {query_find, limit, page, options} = queryApi(req.query);

  CashflowConfig.findAndCountAll({ ...options, where: query_find })
    .then(d => {
      const data = bodyJsonGet(d, options['limit'], page)
      res.status(200).send({ ...data })
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
};

const getValueGlobal = (req, res) => {
  const {query_find, limit, page, options} = queryApi(req.query);

  ValueGlobal.findAndCountAll({ ...options, where: query_find })
    .then(d => {
      const data = bodyJsonGet(d, options['limit'], page)
      res.status(200).send({ ...data })
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
};

module.exports = {
  createCashflowConfig,
  createValueGlobal,
  getCashflowConfig,
  getValueGlobal
}