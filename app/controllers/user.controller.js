const db = require("../models");
const User = db.user;
const Role = db.role;
const UserRole = db.user_role;
const Person = db.person;
const Op = db.Sequelize.Op;

// Create and Save a new Tutorial
// const create = (req, res) => {

//   const body_data = req.body.data;

//   // Validate request
//   if (!body_data.name) {
//     res.status(400).send({
//       message: "Content can not be empty!"
//     });
//     return;
//   }

//   // Create a Tutorial
//   const user = {
//     ...body_data
//   };

//   // Save Tutorial in the database
//   User.create(user)
//     .then(data => {
//       res.status(200).send({data: data})
//     })
//     .catch(err => {
//       res.status(500).send({
//         message:
//           err.message || "Some error occurred while creating the Tutorial."
//       });
//     });
// };

const create = async(req, res) => {

  const body_data = req.body.data;

  // Validate request
  if (!body_data.email) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  const data_person = await Person.findOne({ where: { email: body_data.email } });
  if (!data_person) {
    res.status(404).send({
      message:
        err.message || "Person data not found"
    });
  }

  // Create a Tutorial
  const user = {
    "personId" : data_person.id,
    "username" : body_data.username,
    "email" : body_data.email,
    "password" : body_data.password,
    "fullname" : data_person.fullname,
  };

  // res.status(200).send({data: user})

  // Save Tutorial in the database
  User.create(user)
    .then(data => {
      res.status(200).send({data: data})
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Tutorial."
      });
    });
};

const get = (req, res) => {
  const username = req.query.username;
  var condition = username ? { username: { [Op.like]: `%${username}%` } } : null;

  User.findAll({ where: condition })
    .then(data => {
      res.status(200).send({data: data})
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
};

const getById = (req, res) => {

  const userId = req.params.id;

  User.findByPk(userId, {include: 'person'})
    .then(data => {
      res.status(200).send({data: data})
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
};

const addRole = (req, res) => {

  const body_data = req.body.data;
  const userId = body_data.userId;
  const roleId = body_data.roleId;

  if (!body_data.name) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  return User.findByPk(userId)
    .then((user) => {
      if (!user) {
        console.log("Tag not found!");
        return null;
      }
      return Role.findByPk(roleId).then((role) => {
        if (!role) {
          console.log("Tutorial not found!");
          return null;
        }

        user.addRoles(role);
        console.log(`>> added User id=${user.id} to Role id=${role.id}`);
        res.status(200).send({data: user})
      });
    })
    .catch((err) => {
      console.log(">> Error while adding Role to User: ", err);
    });
};

const addManyRole = async (req, res) => {
  const body_data = req.body.data;
  const userId = body_data.userId;
  const roleList = body_data.roleList;

  try{
  
    const getUser = await User.findByPk(userId);
    if(!getUser){
      return res.status(404).send({
        message: "User not found"
      });
    }

    let bulkOps = []

    for(let i = 0 ; i < roleList.length; i++){
      const getRole = await Role.findByPk(roleList[i]);

      if(!getRole){
        return res.status(500).send({
          message: "Role not found"
        });
      }

      bulkOps.push({user_id : userId, role_id : roleList[i]})

    }

    const ops = await UserRole.bulkCreate(bulkOps)

    res.status(200).send({data: ops})

  }catch(err){
    res.status(500).send({
      message:
        err.message || "Some error occurred while creating the Tutorial."
    });
  }
}

module.exports = {
  create : create,
  get : get,
  getById : getById,
  addRole : addRole,
  addManyRole : addManyRole
}