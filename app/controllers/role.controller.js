const db = require("../models");
const Role = db.role;
const Op = db.Sequelize.Op;

// Create and Save a new Tutorial
const create = (req, res) => {

  const body_data = req.body.data;

  // Validate request
  if (!body_data.email) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  // Create a Tutorial
  const role = {
    ...body_data
  };

//   Save Tutorial in the database
  Role.create(role)
    .then(data => {
      res.status(200).send({data: data})
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Tutorial."
      });
    });
};

const createBulk = (req, res) => {

  const body_data = req.body.data;

  // Validate request
  if (!body_data) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  // Create a Tutorial
  const role = [
    ...body_data
  ];

//   Save Tutorial in the database
  Role.bulkCreate(role)
    .then(data => {
      res.status(200).send({data: data})
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Tutorial."
      });
    });
};

const get = (req, res) => {
  const name = req.query.name;
  var condition = name ? { name: { [Op.like]: `%${name}%` } } : null;

  Role.findAll({ where: condition })
    .then(data => {
      res.status(200).send({data: data})
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
};

module.exports = {
  create : create,
  get : get,
  createBulk : createBulk
}