const db = require("../models");
const Invoice = db.invoice;
const Person = db.person;
const Op = db.Sequelize.Op;
const {parsingDate, getPeriod} = require("../component/functionComponent");
const {bodyJsonGet, queryApi} = require("../component/standardComponent");

const createInvoice = async(req, res) => {

  const body_data = req.body.data;

  // Validate request
  if (!body_data.amount) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  if (!body_data.nik && !body_data.person_id) {
    res.status(400).send({
      message: "ID Siswa can not be empty!"
    });
    return;
  }
  
  let condition = null

  if(body_data.person_id){
    condition = { where: { person_id: body_data.person_id } }
  }
  if(body_data.nik){
    condition = { where: { nik: body_data.nik } }
  }

  const data_person = await Person.findOne(condition);
  if (!data_person) {
    res.status(404).send({
      message:
        err.message || "Person data not found"
    });
  }

  let invoice_year = body_data.invoice_year;
  let invoice_month = body_data.invoice_month;

  let invoice_period = invoice_month

  if(invoice_month && invoice_year && (invoice_month !== invoice_year )){
    invoice_period = invoice_month+"-"+invoice_year
  }

  const period_now = await getPeriod();

  const invoice_special_type = body_data.special_type ? body_data.special_type : body_data.type +"-"+invoice_period;

  const invoice_name = body_data.name ? body_data.name : invoice_special_type+"-"+data_person.fullname;

  const data_invoice = {
    invoice_id:invoice_special_type+"-"+data_person.fullname+"-"+period_now,
    personId: data_person.id,
    client_id: data_person.person_id,
    client_name: data_person.fullname,
    name: invoice_name,
    type_id: body_data.type_id,
    type: body_data.type,
    special_type: invoice_special_type,
    amount: body_data.amount,
    amount_pay: body_data.amount_pay ? body_data.amount_pay : 0,
    invoice_period : invoice_period,
    invoice_year : body_data.invoice_year,
    invoice_month : body_data.invoice_month,
    status: "BELUM LUNAS",
    remark: body_data.remark,
    period: body_data.period ? body_data.period : period_now,
    createdBy : "admintc@gmail.com",
    updatedBy : "admintc@gmail.com"
  }

  try{

    const invoice_ops = await Invoice.create(data_invoice);

    res.status(200).send({data: invoice_ops})

  }catch(err){
    
    res.status(500).send({
      message:
        err.message || "Some error occurred while creating the Tutorial."
    });
  }
}

const createBulkInvoiceUploader = async(req, res) => {

  const body_data = req.body.data;

  // Validate request
  if (!body_data) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  const header = body_data[0]
  const dataOnly = body_data.slice(1)
  const period_now = await getPeriod()

  let dataOps = []

  for(let i = 0; i < dataOnly.length; i++){

    const data = dataOnly[i];

    const person_nisn = data[header.indexOf('NISN')];
    let invoice_name = data[header.indexOf('invoice_name')];
    const invoice_type = data[header.indexOf('type')];
    const invoice_month = data[header.indexOf('invoice_month')];
    let invoice_year = data[header.indexOf('invoice_year')];
    const invoice_amount = data[header.indexOf('amount')];
    const invoice_remark = data[header.indexOf('remark')];

    let invoice_period = invoice_month

    if(invoice_month && invoice_year && (invoice_month !== invoice_year )){
      invoice_period = invoice_month+"#"+invoice_year
    }

    const data_person = await Person.findOne({ where: {[Op.or]: [
      { person_id: person_nisn } ,
      { nik: person_nisn} 
    ]}});;
    if (!data_person) {
      res.status(404).send({
        message:
          err.message || "Person data not found"
      });
    }

    const invoice_special_type = invoice_type +"-"+invoice_period;

    invoice_name = invoice_name ? invoice_name : invoice_special_type+"-"+data_person.fullname

    const data_invoice = {
      invoice_id: invoice_special_type+"-"+data_person.fullname+"-"+period_now,
      personId: data_person.id,
      client_id: data_person.id,
      client_name: data_person.fullname,
      name: invoice_name,
      type_id: invoice_type,
      type: invoice_type,
      special_type: invoice_special_type,
      amount: invoice_amount,
      amount_pay: 0,
      invoice_period : invoice_period,
      invoice_year : invoice_year,
      invoice_month : invoice_month,
      status: "BELUM LUNAS",
      remark: invoice_remark,
      period: period_now,
      createdBy : "admintc@gmail.com",
      updatedBy : "admintc@gmail.com"
    }

    dataOps.push(data_invoice)
  }

  // res.status(200).send({data: dataOps})

  try{

    const invoice_ops = await Invoice.bulkCreate(dataOps)

    res.status(200).send({data: invoice_ops})

  }catch(err){    
    res.status(500).send({
      message:
        err.message || "Some error occurred while creating the Tutorial."
    });
  }
}

const get = (req, res) => {

  const {query_find, limit, page, options} = queryApi(req.query);

  Invoice.findAndCountAll({ ...options, where: query_find })
    .then(d => {
      const data = bodyJsonGet(d, options['limit'], page)
      res.status(200).send({ ...data })
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
};

const getById = (req, res) => {

  const userId = req.params.id;

  Invoice.findByPk(userId, {include: 'user'})
    .then(data => {
      res.status(200).send({data: data})
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
};

module.exports = {
  createInvoice,
  createBulkInvoiceUploader,
  get,
  getById
}