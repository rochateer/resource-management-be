const db = require("../models");
const Receipt = db.receipt;
const Invoice = db.invoice;
const Transaction = db.transaction;
const Person = db.person;
const Op = db.Sequelize.Op;
const {parsingDate, getPeriod} = require("../component/functionComponent");
const {bodyJsonGet, queryApi} = require("../component/standardComponent");

const updateInvoiceAfterpayment = async(bodyOps) => {
  try{

    for(let i = 0; i < bodyOps.length; i++){
      await Invoice.update({ lastName: "Doe" }, {
        where: {
          lastName: null
        }
      });
    }

  }catch(err){
    throw new Error(err.message || "Some error occurred while update invoice after payment.");
  }
}

module.exports = {
  updateInvoiceAfterpayment
}