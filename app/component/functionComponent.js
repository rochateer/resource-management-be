const parsingDate = (jsondate) => {
  if(jsondate === undefined || jsondate === null){
    return null
  }else{
    let date = new Date(jsondate);
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let dt = date.getDate();

    if (dt < 10) {
      dt = "0" + dt;
    }
    if (month < 10) {
      month = "0" + month;
    }
    return {date : dt.toString().padStart(2, '0'), month : month.toString().padStart(2, '0'), year : year.toString(), yr: year.toString().substring(2)};
  }
};

const getPeriod = async () => {

  const period = await "TA 2023/2024";

  return period

}

function getAge(dateString) {
  var today = new Date();
  var birthDate = new Date(dateString);
  var age = today.getFullYear() - birthDate.getFullYear();
  var m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
  }
  return age;
}

module.exports = {
  parsingDate,
  getPeriod,
  getAge
}