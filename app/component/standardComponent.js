const db = require("../models");
const DEFAULT_MAX_RESULTS = 10
const DEFAULT_PAGE = 1
const Op = db.Sequelize.Op;

const bodyJsonGet = (data, limit, page) => {
  return {data : data.rows, _meta : {
    total : data.count,
    max_results : limit && limit !== null && limit !== undefined ? limit : !limit ? data.count : DEFAULT_MAX_RESULTS,
    page : page ? page : DEFAULT_PAGE
  }}
};

const queryApi = (query) => {

  // {"$or" : [{"type" : "Pembayaran Siswa" }], "name" : {"$ne" : "Uang Buku"}, "value" : {"$like" : "21"}}&sort=type:-1&limit=-1&page=2

  const query_where = query.where ? JSON.parse(query.where) : {};

  let limit = query.limit ? parseInt(query.limit) : DEFAULT_MAX_RESULTS;

  let page = query.page ? parseInt(query.page) : DEFAULT_PAGE;

  let offset = ((page-1)*limit);

  const options = {}

  options['limit'] = limit
    
  options['offset'] = ((page-1)*limit)

  if (limit == -1) {
    delete options['limit']
    delete options['offset']
  }

  if(query.sort){
    const sorting = query.sort.split(":");
    const sort_field = sorting[0];
    const sort_direction = sorting[1] == -1 ? 'DESC' : 'ASC';
    options['order'] = [[sort_field, sort_direction]]
  }
  
  let queryFind = {}

  queryFind = queryPreprocess(query_where)

  return {query_find : queryFind, limit : limit, page : page, offset : offset, options : options }
  
};

function queryPreprocess(obj, queryFind = {}){
  for (const key in obj) {
    if(Array.isArray(obj[key])){
      let array_op = []
      for(let i = 0; i < obj[key].length; i++){
        array_op.push(queryPreprocess(obj[key][i]))
      }
      queryFind[Op[(key.replace('$', ''))]] = array_op
    }
    else if(typeof obj[key] === "object"){
      const obj_keys = Object.keys(obj[key])[0];
      if(Object.keys(obj[key])[0] === "$like"){
        queryFind[key] =  { [Op[(obj_keys.replace('$', ''))]]: `%${obj[key][obj_keys]}%` }
      }else{
        queryFind[key] =  { [Op[(obj_keys.replace('$', ''))]]: `${obj[key][obj_keys]}` }
      }
    }else{
      queryFind[key] = obj[key]
    }
  }
  return queryFind
};


module.exports = {
  bodyJsonGet,
  queryApi
}